from hypothesis import given
from hypothesis.strategies import tuples, integers, composite

from src.spoor import transforms as t


@composite
def even_integers(draw):
    i = draw(
        integers(min_value=20, max_value=8000)
        .filter(lambda i: i % 2 == 0)
        .filter(lambda i: i != 0)
    )
    return i


@given(
    tuples(even_integers(), even_integers()).map(sorted).filter(lambda x: x[0] < x[1])
)
def test_original_size_plus_padding(int_tuple: tuple):
    """
    given chip_size < original_size
    original_size + (padding_size * 2) % chip_size
    should always be 0 for even number parameters.
    """
    chip_size, original_size = int_tuple
    padding = t.par_dos.CreateImageChips.get_padding_size(original_size, chip_size)
    assert (original_size + (padding * 2)) % chip_size == 0
