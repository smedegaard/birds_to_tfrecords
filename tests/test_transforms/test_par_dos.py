import unittest
import numpy as np
import apache_beam as beam
from apache_beam.testing.test_pipeline import TestPipeline
from apache_beam.testing.util import assert_that
from apache_beam.testing.util import equal_to

from src.spoor.transforms import par_dos


VIDEO_FRAME_TUPLES = [
    (16349, 393, np.random.rand(3)),
    (17497, 517, np.random.rand(3)),
    (18874, 693, np.random.rand(3)),
    (19847, 717, np.random.rand(3)),
]


class FormatPredictionsTest(unittest.TestCase):
    def test_flatten_pardition(self):
        with TestPipeline() as pipeline:
            count = (
                pipeline
                | beam.Create(VIDEO_FRAME_TUPLES)
                | beam.ParDo(par_dos.FlattenPrediction())
                | beam.combiners.Count.Globally()
            )

            assert_that(count, equal_to([12]))
