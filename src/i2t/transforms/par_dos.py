import os
import sys
import traceback
import re
import hashlib
from typing import Dict
import apache_beam as beam
import tensorflow as tf

# from .tfrecord_util import (
#     bytes_feature,
#     bytes_list_feature,
#     int64_feature,
#     int64_list_feature,
#     float_list_feature,
# )


class SplitAnnotations(beam.DoFn):
    def process(self, input):
        line = re.split("IMG", input)
        return line


class AnnotationsToDict(beam.DoFn):
    def process(self, element):
        classes = {
            "b": 0,
            "n": 1,
            "u": 2,
        }

        class_text = {0: "bird", 1: "non-bird", 2: "unidentified"}

        elements = element.split("\n")
        annotations = {}
        annotations["image_name"] = "IMG" + elements[0]
        annotations["nae_annotations"] = []
        for a in elements[1:-1]:
            words = a.split(",")
            feature = classes[words[4]]
            if feature == 0:
                d = {
                    "x1": int(words[0]),
                    "y1": int(words[1]),
                    "w1": int(words[2]),
                    "h1": int(words[3]),
                    "class/label": feature,
                    "class/text": class_text[feature],
                }
                annotations["nae_annotations"].append(d)

        yield annotations


class ImageToTfExample(beam.DoFn):
    def __init__(self, images_dir):
        self.images_dir = images_dir
        self.new_dimensions = {"height": 640, "width": 640}

    def process(self, element):
        image_name = element["image_name"]
        path = os.path.join(self.images_dir, element["image_name"])
        try:
            image_raw = get_image_content(path)
            sha256_hash = hashlib.sha256(image_raw).hexdigest().encode("utf-8")
            image_decoded = tf.image.decode_jpeg(image_raw)
            org_height, org_width, channels = image_decoded.shape

            # Calc normalized bounding box

            # The line of "x1,y1,w1,h1,(b, n, or u)" means a bounding box whose
            # left-top point is (x1, y1), width is w1 and height is h1
            # http://bird.nae-lab.org/dataset/

            # Tensorflow Object detection also uses top left corner as (0,0)

            for anno in element["nae_annotations"]:
                label_list = []
                class_text_list = []
                xmax_list = []
                ymax_list = []
                xmin_list = []
                ymin_list = []

                # crop randomly around bounding box.
                origo_y = tf.random.uniform(
                    (),
                    int(anno["y1"] - self.new_dimensions["height"] + anno["h1"]),
                    int(anno["y1"]),
                    dtype=tf.int32,
                )

                origo_x = tf.random.uniform(
                    (),
                    int(anno["x1"] - self.new_dimensions["width"] + anno["w1"]),
                    int(anno["x1"]),
                    dtype=tf.int32,
                )

                # add padding if the crop box goes outside of the original image
                delta_width = self.new_dimensions["width"] - (
                    org_width - origo_x.numpy()
                )
                delta_height = self.new_dimensions["height"] - (
                    org_height - origo_y.numpy()
                )

                padding = calc_padding(delta_width, delta_height, self.new_dimensions)
                image_decoded_padded = None
                if padding > 0:
                    target_height = int(org_height + (2 * padding))
                    target_width = int(org_width + (2 * padding))
                    image_decoded_padded = tf.image.pad_to_bounding_box(
                        image=image_decoded,
                        offset_height=padding,
                        offset_width=padding,
                        target_height=target_height,
                        target_width=target_width,
                    )

                image_to_use = (
                    image_decoded_padded
                    if image_decoded_padded is not None
                    else image_decoded
                )

                new_origo = {"y": origo_y + padding, "x": origo_x + padding}
                image_cropped = tf.image.crop_to_bounding_box(
                    image_to_use,
                    new_origo["y"],
                    new_origo["x"],
                    self.new_dimensions["height"],
                    self.new_dimensions["width"],
                )

                image_cropped_encoded = tf.io.encode_jpeg(image_cropped)
                cropped_height, cropped_width, _c = image_cropped.shape

                # y values increase towards the bottom of the image

                for bb in element["nae_annotations"]:
                    ymin = float(bb["y1"] - origo_y) / float(cropped_height)
                    xmin = float(bb["x1"] - origo_x) / float(cropped_width)

                    ymax = float(bb["y1"] + bb["h1"] - origo_y) / float(cropped_height)
                    xmax = float(bb["x1"] + bb["w1"] - origo_x) / float(cropped_width)

                    assert ymin < ymax
                    assert xmin < xmax

                    # if there are other bounding boxes in the crop, add them
                    # bounding_boxes = [anno]
                    if (
                        ymin >= 0.0
                        and ymin <= 1.0
                        and xmin >= 0.0
                        and xmin <= 1.0
                        and ymax >= 0.0
                        and ymax <= 1.0
                        and xmax >= 0.0
                        and xmax <= 1.0
                    ):

                        xmax_list.append(xmax)
                        ymax_list.append(ymax)
                        ymin_list.append(ymin)
                        xmin_list.append(xmin)
                        label_list.append(bb["class/label"])
                        class_text_list.append(bb["class/text"].encode("utf-8"))

                # FORMAT:
                # https://cloud.google.com/ai-platform/training/docs/algorithms/object-detection#required_input_format
                # https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md

                features = tf.train.Features(
                    feature={
                        "image/encoded": bytes_feature(image_cropped_encoded.numpy()),
                        "image/object/class/label": int64_list_feature(label_list),
                        "image/object/class/text": bytes_list_feature(class_text_list),
                        "image/object/bbox/xmin": float_list_feature(xmin_list),
                        "image/object/bbox/xmax": float_list_feature(xmax_list),
                        "image/object/bbox/ymin": float_list_feature(ymin_list),
                        "image/object/bbox/ymax": float_list_feature(ymax_list),
                        "image/filename": bytes_feature(image_name.encode()),
                        "image/hash/sha256": bytes_feature(value=sha256_hash),
                        "image/height": int64_feature(cropped_height),
                        "image/width": int64_feature(cropped_width),
                        "image/num_channels": int64_feature(channels),
                    }
                )

                example = tf.train.Example(features=features)
                yield beam.pvalue.TaggedOutput("examples", example)
        except Exception as e:
            yield beam.pvalue.TaggedOutput("failed", f"{path},{format_exception(e)}")


def int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def int64_list_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def bytes_list_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def float_list_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def get_image_content(path: str) -> bytes:
    if path[0:5] == "gs://":
        image_raw = beam.io.gcp.gcsio.GcsIO().open(path, "rb").read()
    else:
        image_raw = open(path, "rb").read()
    return image_raw


def format_exception(e):
    exception_list = traceback.format_stack()
    exception_list = exception_list[:-2]
    exception_list.extend(traceback.format_tb(sys.exc_info()[2]))
    exception_list.extend(
        traceback.format_exception_only(sys.exc_info()[0], sys.exc_info()[1])
    )

    exception_str = "Traceback (most recent call last):\n"
    exception_str += "".join(exception_list)
    # Removing the last \n
    exception_str = exception_str[:-1]

    return exception_str


def calc_padding(delta_width: int, delta_height: int, new_dimensions: tuple) -> int:
    if delta_width < 0 and delta_height < 0:
        return 0

    delta_max = max(delta_width, delta_height)
    if delta_width == delta_max:
        return delta_width

    if delta_height == delta_max:
        return delta_height

    return 0


def is_in_crop(
    potential_neighbour: tuple, origo_y: int, origo_x: int, new_dimensions: tuple
) -> bool:
    y_min_in_crop = potential_neighbour["y1"] > origo_y
    x_min_in_crop = potential_neighbour["x1"] > origo_x

    y_max_in_crop = (
        potential_neighbour["y1"] + potential_neighbour["h1"]
        < origo_y + new_dimensions["height"]
    )
    x_max_in_crop = (
        potential_neighbour["x1"] + potential_neighbour["w1"]
        < origo_x + new_dimensions["width"]
    )

    return y_min_in_crop and y_max_in_crop and x_min_in_crop and x_max_in_crop
