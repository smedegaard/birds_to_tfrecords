def prediction_schema():
    table_schema = {
        "fields": [
            {"name": "class", "type": "STRING", "mode": "REQUIRED"},
            {"name": "time_in_mili", "type": "INTEGER", "mode": "REQUIRED"},
            {"name": "frame_number", "type": "INTEGER", "mode": "REQUIRED"},
            {"name": "bounding_box_coord_0", "type": "FLOAT", "mode": "REQUIRED"},
            {"name": "bounding_box_coord_1", "type": "FLOAT", "mode": "REQUIRED"},
            {"name": "bounding_box_coord_2", "type": "FLOAT", "mode": "REQUIRED"},
            {"name": "bounding_box_coord_3", "type": "FLOAT", "mode": "REQUIRED"},
            {"name": "confidence", "type": "FLOAT", "mode": "REQUIRED"},
        ]
    }

    return table_schema
