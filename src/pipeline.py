import os
import argparse
import logging
from datetime import datetime

import apache_beam as beam
from apache_beam.io import fileio
from apache_beam.io.gcp import gcsio
from apache_beam.io.gcp.internal.clients import bigquery

from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.pvalue import AsSingleton

import tensorflow as tf

from i2t.transforms import par_dos


def run(
    pipeline_args: dict,
    images_dir: str,
    output_dir: str,
    annotations_dir: str,
    num_shards: int,
) -> None:

    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True

    with beam.Pipeline(options=pipeline_options) as pipeline:
        annotation_files = os.path.join(annotations_dir, "*.txt")
        converted_images = (
            pipeline
            | "Match annotation files" >> fileio.MatchFiles(annotation_files)
            | "Read annotation files" >> fileio.ReadMatches()
            | "Reshuffle" >> beam.Reshuffle()
            | "Get Content" >> beam.Map(lambda x: x.read_utf8())
            | "Split lines" >> beam.ParDo(par_dos.SplitAnnotations())
            | "Filter Empty" >> beam.Filter(lambda line: len(line) > 0)
            | "NAE annotations to dicts" >> beam.ParDo(par_dos.AnnotationsToDict())
            | "To TF examples"
            >> beam.ParDo(par_dos.ImageToTfExample(images_dir)).with_outputs()
        )

        if output_dir[0:5] == "gs://":
            (
                converted_images.failed
                | "Write Local failures file"
                >> beam.io.fileio.WriteToFiles(
                    path=output_dir,
                    destination="csv",
                    file_naming=beam.io.fileio.destination_prefix_naming(),
                )
            )
        else:
            (
                converted_images.failed
                | "Write Local failures file"
                >> beam.io.textio.WriteToText(
                    file_path_prefix=output_dir,
                    shard_name_template="failed-S",
                    file_name_suffix=".csv",
                    append_trailing_newlines=True,
                    header="path,exception",
                )
            )

        (
            converted_images.examples
            | "Write TFRecords"
            >> beam.io.tfrecordio.WriteToTFRecord(
                output_dir,
                coder=beam.coders.ProtoCoder(tf.train.Example),
                file_name_suffix=".tfrecord",
                num_shards=num_shards,
                shard_name_template="BIRD-SSSSS-of-NNNNN",
            )
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--output_dir",
        type=str,
        dest="output_dir",
        default="gs://nae-lab/tf_records/",
        help="Output path to write the csv and html files to.",
    )

    parser.add_argument(
        "--images_dir",
        type=str,
        dest="images_dir",
        help="input path to read the image files from",
    )

    parser.add_argument(
        "--annotations_dir",
        required=True,
        type=str,
        dest="annotations_dir",
        help="path to directory to read annotation files from",
    )

    parser.add_argument(
        "--num_shards",
        type=int,
        dest="num_shards",
        help="number of TFRecord files that wil be written",
        default=3,
    )
    parser.add_argument("--runner", default="DirectRunner", dest="runner")

    known_args, pipeline_args = parser.parse_known_args()
    job_name = f"three-little-birds-{datetime.now().strftime('%y%m%d-%H%M%S')}"
    pipeline_args.extend(
        [
            f"--runner={known_args.runner}",
            "--project=three-little-birds-312706",
            "--region=europe-west6",
            "--staging_location=gs://nae-lab/staging/",
            "--temp_location=gs://nae-lab/temp/",
            f"--job_name={job_name}",
            "--setup_file=./setup.py",
        ]
    )

    print("\n------------------------")
    print(f"JOB NAME: {job_name}")
    print(f"WRITING TO: {known_args.output_dir}")
    print("------------------------")

    run(
        pipeline_args,
        images_dir=known_args.images_dir,
        output_dir=known_args.output_dir,
        annotations_dir=known_args.annotations_dir,
        num_shards=known_args.num_shards,
    )
