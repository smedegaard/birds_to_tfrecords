import setuptools

PACKAGE_NAME = "birds-to-tfrecords"

REQUIRED_PACKAGES = [
    "apache-beam[gcp]==2.29.0",
    "python-snappy==0.6.0",
    "tensorflow==2.5.0"
]

setuptools.setup(
    name=PACKAGE_NAME,
    version="0.1",
    install_requires=REQUIRED_PACKAGES,
    packages=setuptools.find_packages(),
    extras_require={"dev": ["pytest", "pytest-pep8", "pytest-cov", "hypothesis"]},
)
